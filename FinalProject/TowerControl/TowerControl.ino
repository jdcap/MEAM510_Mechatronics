/* Based on WebSocketServer_LEDcontrol.ino  Created on: 26.11.2015 From: http://arduino-er.blogspot.com/2016/05/nodemcuesp8266-implement.html
 *
 *  Updated 2016/07/12 By: Paul Stegall
 *  Reads in button and switch infromation and displays values on a led bar, wiring can be found in JudgeBoardV2.svg
 *  Websockets not currently utilized.
 *
 *  https://github.com/Links2004/arduinoWebSockets
 *
 */
//Modified to act as AP
//arduino-er.blogspot.com

#include <Arduino.h>

#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <WebSocketsClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <Hash.h>
// ********** LED STUFF ********** //
#define FASTLED_ESP8266_RAW_PIN_ORDER
#include "FastLED.h"

// How many leds in your strip?
#define NUM_LEDS 25
#define DATA_PIN D1
#define CLOCK_PIN D2

// Define the array of leds
CRGB leds[NUM_LEDS];
// **********  MACROS  ********** //

#define MIN(X, Y)  ((X) < (Y) ? (X) : (Y))
#define MAX(X, Y)  ((X) > (Y) ? (X) : (Y))
#define SGN(X)     ((X > 0) - (X < 0))

#define HIT_BUTTON1   D6
#define HIT_BUTTON2   D7

#define USE_SERIAL Serial
#define RED_B  1
#define BLUE_B 0

//TUNING PARAMETERS
//*********************************************************
#define TOWER_SWITCH_PER  2000     //milliseconds after capture before which the opposing team can recapture
#define COOLDOWN_TIME     3000     //milliseconds before uncontested tower starts to reduce to zero
#define HIT_SEND_PER      1000       //milliseconds between hit sends
#define DOWN_MULTI        3        //how many times faster gauge drops when the opposing team presses the button
#define GAUGE_MAX         8000     //milliseconds to fill gauge and capture tower

// Boolean, disables controlling teams button, NERFING defense causes PINGPONGing (comment to disable)
#define NERF_DEFENSE()    //if (towerState!=0){ buttonSt[abs(towerState-1) / 2] = 0; }
//NERFS Defense by allowing them to pause the gauge but not to affect the value
#define HOLD_NO_DROP()    ((teamDir*towerState) !=1)
//*********************************************************

// Tower gauge parameters
int towerGauge = 0;
int towerState = 0;//-1 is BLUE, 1 is RED

// Communication string
char hitString [7];


ESP8266WiFiMulti WiFiMulti;

ESP8266WebServer server (80);
WebSocketsClient webSocket;

void webSocketEvent(WStype_t type, uint8_t * payload, size_t length) {

    switch(type) {
        case WStype_DISCONNECTED:
            USE_SERIAL.printf("[WSc] Disconnected!\n");
            break;
        case WStype_CONNECTED: {
            USE_SERIAL.printf("[WSc] Connected to url: %s\n",  payload);

            // send message to server when Connected
            webSocket.sendTXT("Connected");
        }
            break;
        case WStype_TEXT:
            USE_SERIAL.printf("[WSc] get Text: %s\n", payload);

            break;
    }

}



void setup() {
    //USE_SERIAL.begin(921600);
    USE_SERIAL.begin(115200);

    //USE_SERIAL.setDebugOutput(true);

    USE_SERIAL.println();
    USE_SERIAL.println();
    delay(1000);

    pinMode(HIT_BUTTON1, INPUT_PULLUP);
    pinMode(HIT_BUTTON2, INPUT_PULLUP);
    pinMode(LED_BUILTIN, OUTPUT);
    FastLED.addLeds<APA102, DATA_PIN, CLOCK_PIN, BGR>(leds, NUM_LEDS);
    digitalWrite(LED_BUILTIN, LOW);

    WiFiMulti.addAP("Central", "RobCarpick");

    //WiFi.disconnect();
    int count=0;
//    while(WiFiMulti.run() != WL_CONNECTED) {
//        count ++;
//        delay(100);
//        if( count > 50){
//          break;
//        }
//    }
    if(WiFiMulti.run() == WL_CONNECTED) {
        Serial.println("");
        Serial.println("WiFi connected");
        Serial.println("IP address: ");
        Serial.println(WiFi.localIP());
    }

    webSocket.begin("10.0.0.2", 81);
    webSocket.onEvent(webSocketEvent);

}

void loop() {
//    webSocket.loop();

    //Run LED strip
    tower_handle();  //Counts up and updates the towerGauge and towerState
    linear_growth(towerGauge/(GAUGE_MAX/100), towerState, leds);
    FastLED.show();
    tower_send();
    delay(50);  // wait a bit
}
//**********************************
//TOWER_SEND()
void tower_send(){

    // Hit timing static
    static unsigned long sentTime  = 0;
    if (towerState == 0) return;
    //Send damage information to central
    if (millis()-sentTime > HIT_SEND_PER){
        sentTime = millis();
        if (towerState == 1 ){
            sprintf(hitString,"%s10000","R5");
        }else if(towerState == -1){
            sprintf(hitString,"%s10000","B5");
        }
        webSocket.sendTXT(hitString);
    }

}

//**********************************
//TOWER_HANDLE()
void tower_handle() {
    // Static time variables for handling how the tower is captured.
    static unsigned long        timeLast  = millis();
    static unsigned long  towerSwitchTime = -TOWER_SWITCH_PER;
    static unsigned long    cooldownTime  = millis();
    static unsigned int    cooldownFlag  = 0;

    // Save button states, negated because of pullups
    int buttonSt[2] = {!digitalRead(HIT_BUTTON1),!digitalRead(HIT_BUTTON2)};

    NERF_DEFENSE();

    int timePast = millis()-timeLast;
    timeLast = millis();

    // If both buttons are pressed, nothing happens
    if (buttonSt[RED_B]&&buttonSt[BLUE_B]) return;

    // If the tower just switched, ignore inputs for TOWER_SWITCH_PER ms
    if (millis() - towerSwitchTime < TOWER_SWITCH_PER) return;

    // Change in gauge sign/direction
    /* teamDir = 1 if red is pressed, -1 if blue is pressed,
     *           0 if neither is pressed, 0 if both are pressed (handled above)*/
    int teamDir = (buttonSt[RED_B]-buttonSt[BLUE_B]);

    // If only one button is pressed
    /* Many of the operations below use the sign of the towerGauge and teamDir to make decisions about state
     * (I think I over did this, should be moved back to if or switch statement, I just hate repeating code.-Diego)*/
    if (teamDir && HOLD_NO_DROP()){

        // Check if opposing team from current button press partially captured the tower
        if (towerGauge * teamDir < 0){
            /* Negative implies that the towerGauge was pushed partially by the other team
             * since otherwise signs would match and cancel.
             * If this is the case, we multiply the tower rate by DOWN_MULTI
             * allowing a team to quickly clear the opposing teams partial capture
             * to facilitate their capture. This does not go past zero in this mode.*/
            towerGauge += DOWN_MULTI * teamDir * timePast;
            towerGauge = teamDir * MIN( teamDir * towerGauge, 0);
        }else{
            /*Normal capture counts the millis the team holds the button in towerGauge */
            towerGauge += teamDir * timePast;
        }
        //Clears the cooldown flag if any button is pressed
        cooldownFlag = 0;
    }
    /* If neither button is being pressed but the gauge is partially engaged,
     * start a cooldown timer after which the tower will drop back down to zero.*/
    else if(towerGauge != 0){
        //Start timer.
        if(!cooldownFlag){
            cooldownFlag = 1;
            cooldownTime = millis();
        }
        //If timer is up, lower the towerGauge at the normal 1/ms
        else if(millis()-cooldownTime > COOLDOWN_TIME){
            towerGauge = timePast > abs(towerGauge) ? 0 : towerGauge-(SGN(towerGauge)*timePast) ;
        }
    }
    // If towerGauge reaches max, set tower capture by that team
    if (abs(towerGauge) >= GAUGE_MAX){
        towerSwitchTime = millis();
        towerState = SGN(towerGauge);
    }
    /* Clamp towerGauge to 0 for current captors,
     * but allow gauge to move for attackers */
    towerGauge = (!(towerState*towerGauge > 0)) * towerGauge;


   USE_SERIAL.print("\tRed Button: \t");
   USE_SERIAL.print(buttonSt[RED_B]);
   USE_SERIAL.print("\tBlue Button: \t");
   USE_SERIAL.println(buttonSt[BLUE_B]);
   USE_SERIAL.print("Tower Gauge: \t");
   USE_SERIAL.println(towerGauge);
   USE_SERIAL.print("\tTime Past: \t");
   USE_SERIAL.print(timePast);
}

//**********************************
//LINEAR_GROWTH()
//  INPUTS:
//  percentage: -100 <= percentage <= 100
//       state:  1 for red capture or -1 for blue capture
//        leds: LED array
void linear_growth(int percentage, int state, CRGB leds[] ){
    /* These probably don't need to be static,
     * but should be a tiny bit faster in exchange for some memory usage.*/
    static byte maxBrightness = 255;
    static byte      saturate = .5 * maxBrightness;
    static byte         slope = .1 * maxBrightness;
    static int         growth = saturate + NUM_LEDS * slope;

    uint8_t i = 0;
    int value;

    /* Percentage should be an integer in [-100,100], the sign gives
     * us whether this is red or blue team capturing (1 for red, -1 for blue)*/
    int8_t redOrBlue = (percentage > 0) - (percentage < 0);

    percentage = abs(percentage);

    // Which color? 0 is red, 160 is blue
    uint8_t hue = (redOrBlue < 0) * 160;
    // Which background color?
    uint8_t bgHue = (state < 0) * 160;


    for(i=0;i<NUM_LEDS;i++){
        // CHSV( HUE, SATURATION, VALUE);
        // value is a linear function: value = m x + b
        // but it also handles whether the leds should be blue or red and
        // the switch in direction that comes with that.
        // the constant slope, m =  (- redOrBlue *slope)
        // The intercept,b, depends on the percentage
        // w/ b =( growth * percentage) / 100 - slope * NUM_LEDS*(redOrBlue<0)
        value = (- redOrBlue *slope) * i
              + (growth * percentage) / 100 - slope * NUM_LEDS*(redOrBlue<0) ;
        leds[i] =  CHSV( hue, 255 , MIN( MAX(value, 0), saturate));
        if(state!=0){
            leds[i]  += CHSV( bgHue, 255 ,MIN( MAX(maxBrightness - value, 0),  maxBrightness)) ;
        }
    }
    if (state!=0){
        cannon_ball(leds,state);
    }

}


//**********************************
//CANNON_BALL()
void cannon_ball( CRGB leds[] , int dir){
    static unsigned long cannonTime = millis();
    static uint8_t cannonRate = NUM_LEDS; //# of LEDs traversed per second

    int cannonBallLoc = (dir<0)*(NUM_LEDS-1) + dir *( ((millis()-cannonTime)*cannonRate)/1000);// time * leds per millisecond

    //Deal with cannonball reset
    if ( (cannonBallLoc >= NUM_LEDS) || (cannonBallLoc < 0) ){
      cannonTime = millis();
      cannonBallLoc = (dir<0) * (NUM_LEDS-1);
    }
    leds[cannonBallLoc]=CHSV(0,0,255); //The cannon ball is WHITE
}
